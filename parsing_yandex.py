import logging
import re
import time
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.by import By
from selenium import webdriver
from aiogram import Bot, Dispatcher, executor, types

# Объект бота


bot = Bot(token="YOUR_API_TOKEN")
# Диспетчер для бота
dp = Dispatcher(bot)
# Включаем логирование, чтобы не пропустить важные сообщения
logging.basicConfig(level=logging.INFO)


class Yandex:
    site_of_image = "https://yandex.ru/images/search?text="
    site_of_search = "https://yandex.ru/search/?lr=6&text="
    number_of_search = 5
    iter = True


stop_word = ['стоп', 'стой', 'остановись', 'хватит', 'прекрати']


# Хэндлер на команду /test1
@dp.message_handler(commands=['start'])
async def send_welcome(message: types.Message):
    await message.answer("Привет, напиши мне запрос. Я пришлю тебе картинки\n"
                         "Количество элементов поиска можно задать через *\n"
                         "Бот постоянно дорабатывается")


@dp.message_handler()
async def stop_iter(message: types.Message):
    message_user = message.text.strip().lower()
    if message_user in stop_word:
        Yandex.iter = False
    else:
        Yandex.iter = True
        await send_pictures(message)



async def send_pictures(message: types.Message):
    options = FirefoxOptions()
    options.add_argument("--headless")
    driver = webdriver.Firefox(r'C:/ddd/PYprojects/parpisng_yandex/', options=options)
    message_user = message.text
    url_image = []
    regex = r"\*"

    if (re.search(regex, message_user)):
        user_m = message_user.split('*')
        switch_pages = int(user_m[1])
        Yandex.number_of_search = switch_pages
        message_user = user_m[0]
    else:
        Yandex.number_of_search = 5

    if (re.search('поиск', message_user.strip().lower())):
        message_user = message_user[5:]
        driver.get(f'{Yandex.site_of_search}{message_user}')
        message_to_user = 'сайты'
        await message.answer(
            f'Ищу {message_to_user} по запросу "{message_user}" в количестве {Yandex.number_of_search} шт')
        time.sleep(2)
        for i in range(1, Yandex.number_of_search + 1):
            try:
                el = driver.find_element(By.XPATH,
                                     f"/html/body/div[3]/div[2]/div[2]/div[1]/div[1]/ul/li[{i}]/div/div[1]/a/h2/span")
                el2 = driver.find_element(By.XPATH,
                                      f"/html/body/div[3]/div[2]/div[2]/div[1]/div[1]/ul/li[{i}]/div/div[1]/a")

                await message.answer(f"{el.text}\n{el2.get_attribute('href')}")
            except:
                pass


    else:
        message_to_user = 'картинки'
        driver.get(f'{Yandex.site_of_image}{message_user}')
        await message.answer(
            f'Ищу {message_to_user} по запросу "{message_user}" в количестве {Yandex.number_of_search} шт'
            f'\nДля остановки напиши мне стоп или хватит и т.д.')
        time.sleep(2)


    print(message_user)
    try:

        try:
            first_click = driver.find_element(By.XPATH, "/html/body/div[3]/div[2]/div[1]/div[1]/div/div[1]/div/a")
            webdriver.ActionChains(driver).click_and_hold(first_click).perform()
            webdriver.ActionChains(driver).release().perform()
        except:
            first_click = driver.find_element(By.XPATH, "/html/body/div[3]/div[2]/div[1]/div[2]/div/div[2]/div/a")
            webdriver.ActionChains(driver).click_and_hold(first_click).perform()
            webdriver.ActionChains(driver).release().perform()

    except:
        Yandex.iter = False
    try:
        time.sleep(2)
        for i in range(0, Yandex.number_of_search):
            if Yandex.iter:
                try:
                    try:
                        el = driver.find_element(By.XPATH,
                                                 "/html/body/div[13]/div[2]/div/div/div/div[3]/div/div[3]/div/div/div[1]/div[4]/div[1]/a")
                    except:
                        el = driver.find_element(By.XPATH,
                                                 "/html/body/div[13]/div[2]/div/div/div/div[3]/div/div[3]/div/div/div[1]/div[3]/div[1]/a")
                    url_image.append(el.get_attribute("href"))

                    el2 = driver.find_element(By.XPATH,
                                              "/html/body/div[13]/div[2]/div/div/div/div[3]/div/div[2]/div[1]/div[4]/i")
                    webdriver.ActionChains(driver).click_and_hold(el2).perform()
                    webdriver.ActionChains(driver).release().perform()
                    await types.ChatActions.upload_photo()
                    media = types.MediaGroup()
                    media.attach_photo(url_image[i])
                    await message.answer_media_group(media=media)

                except:
                    pass
    except:
        print()
    await message.answer("Я готов служить вам дальше, а вообще - вечно")
    driver.close()


if __name__ == "__main__":
    # Запуск бота
    executor.start_polling(dp, skip_updates=True)
